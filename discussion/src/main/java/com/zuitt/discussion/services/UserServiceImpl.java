package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.models.User;

import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

// Will allow us to use the CRUD repository method inherited from the CRUDRepository
@Service
public class UserServiceImpl implements UserService{

    // An object cannot be instantiated from interfaces.
    // @Autowired allows us to use the interface as if it was an instance of an object and allows us to use the methods from the CRUDRespository
    @Autowired
    private UserRepository userRepository;
    // Create user
    public void createUser(User user){
        userRepository.save(user);
    }

    // Get all user
    public Iterable<User> getUsername(){
        return userRepository.findAll();
    }

    // Delete user
    public ResponseEntity deleteUser(Long id){
        userRepository.deleteById(id);
        return new ResponseEntity<>("User Deleted successfully", HttpStatus.OK);
    }

    // Update a user
    public ResponseEntity updateUser(Long id, User user){
        // Find the user to update
        User userForUpdate = userRepository.findById(id).get();
        // Updating the username and password
        userForUpdate.setUsername(user.getUsername());
        userForUpdate.setPassword(user.getPassword());
        // Saving and Updating a user
        userRepository.save(userForUpdate);
        return new ResponseEntity<>("User updated Successfully",HttpStatus.OK);
    }
}

